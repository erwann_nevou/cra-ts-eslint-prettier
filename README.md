# CRA-TS-ESLINT-PRETTIER

Base d'un projet react typescript avec ESLint & prettier & gitlab-ci

# Configuration

Installer l'extension prettier et ajouter la configuration suivante à VSC :

```json
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "[javascript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode"
    },
    "[typescript]": {
        "editor.defaultFormatter": "esbenp.prettier-vscode",
        "editor.formatOnSave": true,
    },
    "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
    },
    "editor.formatOnSave": true,
```
